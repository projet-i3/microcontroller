#include "main.h"
#include <stdlib.h>

static void MX_GPIO_Init(void);

/**
 * Change the floor request state to 1 or 2 depending on the button.
 * @param requested_floors Array of floors requests state.
 * @param floor_to_add The floor to add.
 * @param from_internal_button 1 if from internal button, 0 otherwise.
 */
void add_requested_floor(floor_command requested_floors[], int floor_to_add, int from_internal_button) {
    if (from_internal_button) {
        requested_floors[floor_to_add].requested = 2;
    } else if (requested_floors[floor_to_add].requested == 0) { // We don't want to change it if requested == 2.
        requested_floors[floor_to_add].requested = 1;
    }
}

/**
 * Change the floor request state to 0.
 * @param requested_floors Array of floors requests state.
 * @param floor_to_remove The floor to remove.
 */
void remove_requested_floor(floor_command requested_floors[], int floor_to_remove) {
    requested_floors[floor_to_remove].requested = 0;
}

/**
 * Give the destination floor.
 * @param requested_floors Array of floors requests state.
 * @param current_floor The current floor of the elevator.
 * @param moving_state The current direction of the elevator.
 * @return The destination floor.
 */
int get_next_floor(floor_command requested_floors[], int current_floor, moving_states moving_state) {
    int floor_min = EMPTY, distance_min = NUMBER_FLOORS, found_min = 0;
    moving_states direction;

    // First we check if there is a floor requested from inside the elevator (requested==2).
    for (int i = 0; i < NUMBER_FLOORS; i++) {
        if (requested_floors[i].requested) {
            requested_floors[i].distance = i - current_floor;
            if (requested_floors[i].requested == 2 && abs(requested_floors[i].distance) < distance_min) {
                distance_min = abs(requested_floors[i].distance);
                floor_min = i;
                if (requested_floors[i].distance < 0) {
                    direction = DOWN;
                } else {
                    direction = UP;
                }
                // It has to be the same direction or IDLE, we don't want the elevator to suddenly change direction.
                if (moving_state == direction || moving_state == IDLE) {
                    found_min = 1;
                }
            }
        }
    }
    // If there is none we search a floor requested from outside the elevator (requested==1).
    if (!found_min) {
        for (int i = 0; i < NUMBER_FLOORS; i++) {
            if (requested_floors[i].requested == 1 && abs(requested_floors[i].distance) < distance_min) {
                distance_min = abs(requested_floors[i].distance);
                floor_min = i;
                if (requested_floors[i].distance < 0) {
                    direction = DOWN;
                } else {
                    direction = UP;
                }
            }
        }
    }
    return floor_min;
}

/**
 * Check buttons states.
 * @param requested_floors Array of floors requests state.
 */
void floor_user_input(floor_command requested_floors[]) {
    // Internals buttons.
    if (HAL_GPIO_ReadPin(BTN_INTERNAL_FLOOR_0_PORT, BTN_INTERNAL_FLOOR_0_PIN)) {
        add_requested_floor(requested_floors, 0, 1);
    }
    if (HAL_GPIO_ReadPin(BTN_INTERNAL_FLOOR_1_PORT, BTN_INTERNAL_FLOOR_1_PIN)) {
        add_requested_floor(requested_floors, 1, 1);
    }
    if (HAL_GPIO_ReadPin(BTN_INTERNAL_FLOOR_2_PORT, BTN_INTERNAL_FLOOR_2_PIN)) {
        add_requested_floor(requested_floors, 2, 1);
    }
    if (HAL_GPIO_ReadPin(BTN_INTERNAL_FLOOR_3_PORT, BTN_INTERNAL_FLOOR_3_PIN)) {
        add_requested_floor(requested_floors, 3, 1);
    }

    // Externals buttons.
    if (HAL_GPIO_ReadPin(BTN_EXTERNAL_FLOOR_0_PORT, BTN_EXTERNAL_FLOOR_0_PIN)) {
        add_requested_floor(requested_floors, 0, 0);
    }
    if (HAL_GPIO_ReadPin(BTN_EXTERNAL_FLOOR_1_PORT, BTN_EXTERNAL_FLOOR_1_PIN)) {
        add_requested_floor(requested_floors, 1, 0);
    }
    if (HAL_GPIO_ReadPin(BTN_EXTERNAL_FLOOR_2_PORT, BTN_EXTERNAL_FLOOR_2_PIN)) {
        add_requested_floor(requested_floors, 2, 0);
    }
    if (HAL_GPIO_ReadPin(BTN_EXTERNAL_FLOOR_3_PORT, BTN_EXTERNAL_FLOOR_3_PIN)) {
        add_requested_floor(requested_floors, 3, 0);
    }
}

/**
 * @brief  The application entry point.
 * @retval int
 */
int main(void) {
    int state = STATE_WAITING;
    int current_floor = 0;
    int destination_floor;
    int still_on_floor = 1;
    int wait_time = 0;
    moving_states moving_state;
    floor_command requested_floors[NUMBER_FLOORS];

    for (int i = 0; i < NUMBER_FLOORS; i++) {
        requested_floors[i].requested = 0;
        requested_floors[i].distance = 0;
    }

    /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
    HAL_Init();

    MX_GPIO_Init();

    while (1) {
        floor_user_input(requested_floors);
        int keep_door_open = HAL_GPIO_ReadPin(BTN_OPEN_DOOR_PORT, BTN_OPEN_DOOR_PIN);

        destination_floor = get_next_floor(requested_floors, current_floor, moving_state);
        switch (state) {
        case STATE_WAITING:
            moving_state = IDLE;
            if (destination_floor != EMPTY) {
                state = STATE_MOVING_UP_DOWN;
            }
            break;
        case STATE_MOVING_UP_DOWN:
            if (!HAL_GPIO_ReadPin(CAPTOR_FLOOR_PORT, CAPTOR_FLOOR_PIN)) {
                if (!still_on_floor) {
                    if (moving_state == UP) {
                        current_floor++;
                    } else {
                        current_floor--;
                    }
                    still_on_floor = 1;
                }
            } else {
                still_on_floor = 0;
            }

            if (current_floor < destination_floor) {
                moving_state = UP;
                HAL_GPIO_WritePin(LIGHT_UP_PORT, LIGHT_UP_PIN, 1);
                HAL_GPIO_WritePin(MOTORS_DIR_PORT, MOTORS_DIR_PIN, 1);
                HAL_GPIO_TogglePin(MOTOR_UP_DOWN_STEP_PORT, MOTOR_UP_DOWN_STEP_PIN);
                for (int i = 0; i < INTERSTEP_TIME_MOTOR_UP_DOWN; i++);
            } else if (current_floor > destination_floor) {
                moving_state = DOWN;
                HAL_GPIO_WritePin(LIGHT_DOWN_PORT, LIGHT_DOWN_PIN, 1);
                HAL_GPIO_WritePin(MOTORS_DIR_PORT, MOTORS_DIR_PIN, 0);
                HAL_GPIO_TogglePin(MOTOR_UP_DOWN_STEP_PORT, MOTOR_UP_DOWN_STEP_PIN);
                for (int i = 0; i < INTERSTEP_TIME_MOTOR_UP_DOWN; i++);
            }
            if (current_floor == destination_floor && still_on_floor) {
                HAL_GPIO_WritePin(LIGHT_UP_PORT, LIGHT_UP_PIN, 0);
                HAL_GPIO_WritePin(LIGHT_DOWN_PORT, LIGHT_DOWN_PIN, 0);
                state = STATE_OPENING_DOOR;
            }
            break;
        case STATE_OPENING_DOOR:
            moving_state = IDLE;
            HAL_GPIO_WritePin(MOTORS_DIR_PORT, MOTORS_DIR_PIN, 1);
            if (HAL_GPIO_ReadPin(CAPTOR_OPEN_DOOR_PORT, CAPTOR_OPEN_DOOR_PIN)) {        // Door not open.
                HAL_GPIO_TogglePin(MOTOR_DOOR_STEP_PORT, MOTOR_DOOR_STEP_PIN);
                for (int i = 0; i < INTERSTEP_TIME_MOTOR_DOOR; i++);
            } else {
                state = STATE_WAITING_DOOR;
            }
            break;
        case STATE_WAITING_DOOR:
            if (keep_door_open) {
                wait_time = 0;
            }
            if (wait_time == DOOR_WAITING_TIME) {
                wait_time = 0;
                state = STATE_CLOSING_DOOR;
            } else {
                wait_time++;
            }
            break;
        case STATE_CLOSING_DOOR:
            HAL_GPIO_WritePin(MOTORS_DIR_PORT, MOTORS_DIR_PIN, 0);
            // If there is an obstacle or the button "Keep door open" is pressed, open the door.
            if (!HAL_GPIO_ReadPin(CAPTOR_OBSTACLE_PORT, CAPTOR_OBSTACLE_PIN) || keep_door_open) {
                state = STATE_OPENING_DOOR;
            } else if (!HAL_GPIO_ReadPin(CAPTOR_CLOSED_DOOR_PORT, CAPTOR_CLOSED_DOOR_PIN)) {        // Door not closed.
                HAL_GPIO_TogglePin(MOTOR_DOOR_STEP_PORT, MOTOR_DOOR_STEP_PIN);
                for (int i = 0; i < INTERSTEP_TIME_MOTOR_DOOR; i++);
            } else {
                state = STATE_WAITING;
            }
            remove_requested_floor(requested_floors, current_floor);
            break;
        }
    }
}

/**
 * @brief GPIO Initialization Function
 * @param None
 * @retval None
 */
static void MX_GPIO_Init(void) {
    GPIO_InitTypeDef GPIO_InitStruct = { 0 };

    /* GPIO Ports Clock Enable */
    __HAL_RCC_GPIOC_CLK_ENABLE();
    __HAL_RCC_GPIOD_CLK_ENABLE();
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();

    // Set Low Speed for all ports.
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;

    GPIO_InitStruct.Pin = MOTORS_DIR_PIN;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_PULLDOWN;
    HAL_GPIO_Init(MOTORS_DIR_PORT, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = MOTOR_UP_DOWN_STEP_PIN;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_PULLDOWN;
    HAL_GPIO_Init(MOTOR_UP_DOWN_STEP_PORT, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = MOTOR_DOOR_STEP_PIN;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_PULLDOWN;
    HAL_GPIO_Init(MOTOR_DOOR_STEP_PORT, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = BTN_EXTERNAL_FLOOR_0_PIN;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLDOWN;
    HAL_GPIO_Init(BTN_EXTERNAL_FLOOR_0_PORT, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = BTN_EXTERNAL_FLOOR_1_PIN;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLDOWN;
    HAL_GPIO_Init(BTN_EXTERNAL_FLOOR_1_PORT, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = BTN_EXTERNAL_FLOOR_2_PIN;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLDOWN;
    HAL_GPIO_Init(BTN_EXTERNAL_FLOOR_2_PORT, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = BTN_EXTERNAL_FLOOR_3_PIN;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLDOWN;
    HAL_GPIO_Init(BTN_EXTERNAL_FLOOR_3_PORT, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = BTN_INTERNAL_FLOOR_0_PIN;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLDOWN;
    HAL_GPIO_Init(BTN_INTERNAL_FLOOR_0_PORT, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = BTN_INTERNAL_FLOOR_1_PIN;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLDOWN;
    HAL_GPIO_Init(BTN_INTERNAL_FLOOR_1_PORT, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = BTN_INTERNAL_FLOOR_2_PIN;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLDOWN;
    HAL_GPIO_Init(BTN_INTERNAL_FLOOR_2_PORT, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = BTN_INTERNAL_FLOOR_3_PIN;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLDOWN;
    HAL_GPIO_Init(BTN_INTERNAL_FLOOR_3_PORT, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = BTN_OPEN_DOOR_PIN;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLDOWN;
    HAL_GPIO_Init(BTN_OPEN_DOOR_PORT, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = LIGHT_UP_PIN;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_PULLDOWN;
    HAL_GPIO_Init(LIGHT_UP_PORT, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = LIGHT_DOWN_PIN;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_PULLDOWN;
    HAL_GPIO_Init(LIGHT_DOWN_PORT, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = CAPTOR_FLOOR_PIN;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    HAL_GPIO_Init(CAPTOR_FLOOR_PORT, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = CAPTOR_OBSTACLE_PIN;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLDOWN;
    HAL_GPIO_Init(CAPTOR_OBSTACLE_PORT, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = CAPTOR_OPEN_DOOR_PIN;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLDOWN;
    HAL_GPIO_Init(CAPTOR_OPEN_DOOR_PORT, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = CAPTOR_CLOSED_DOOR_PIN;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLDOWN;
    HAL_GPIO_Init(CAPTOR_CLOSED_DOOR_PORT, &GPIO_InitStruct);

    /* EXTI interrupt init*/
    HAL_NVIC_SetPriority(EXTI15_10_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);
}

/**
 * @brief  This function is executed in case of error occurrence.
 * @retval None
 */
void Error_Handler(void) {
    /* USER CODE BEGIN Error_Handler_Debug */
    /* User can add his own implementation to report the HAL error return state */
    __disable_irq();
    while (1) {
    }
    /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
