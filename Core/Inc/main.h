#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

#include "stm32f1xx_hal.h"

void Error_Handler(void);

#define USART_TX_Pin GPIO_PIN_2
#define USART_TX_GPIO_Port GPIOA
#define USART_RX_Pin GPIO_PIN_3
#define USART_RX_GPIO_Port GPIOA

// Set the total number of floors of the model.
#define NUMBER_FLOORS 4

// Time before closing the door after it is open.
#define DOOR_WAITING_TIME 100000

// Time between two steps.
#define INTERSTEP_TIME_MOTOR_UP_DOWN 700
#define INTERSTEP_TIME_MOTOR_DOOR 10000

#define EMPTY -1

// Motors
#define MOTORS_DIR_PIN GPIO_PIN_2
#define MOTORS_DIR_PORT GPIOC

#define MOTOR_UP_DOWN_STEP_PIN GPIO_PIN_11
#define MOTOR_UP_DOWN_STEP_PORT GPIOA

#define MOTOR_DOOR_STEP_PIN GPIO_PIN_11
#define MOTOR_DOOR_STEP_PORT GPIOB

// Externals buttons
#define BTN_EXTERNAL_FLOOR_0_PIN GPIO_PIN_8
#define BTN_EXTERNAL_FLOOR_0_PORT GPIOC

#define BTN_EXTERNAL_FLOOR_1_PIN GPIO_PIN_6
#define BTN_EXTERNAL_FLOOR_1_PORT GPIOC

#define BTN_EXTERNAL_FLOOR_2_PIN GPIO_PIN_5
#define BTN_EXTERNAL_FLOOR_2_PORT GPIOC

#define BTN_EXTERNAL_FLOOR_3_PIN GPIO_PIN_3
#define BTN_EXTERNAL_FLOOR_3_PORT GPIOC

// Internals buttons
#define BTN_INTERNAL_FLOOR_0_PIN GPIO_PIN_2
#define BTN_INTERNAL_FLOOR_0_PORT GPIOB

#define BTN_INTERNAL_FLOOR_1_PIN GPIO_PIN_1
#define BTN_INTERNAL_FLOOR_1_PORT GPIOB

#define BTN_INTERNAL_FLOOR_2_PIN GPIO_PIN_15
#define BTN_INTERNAL_FLOOR_2_PORT GPIOB

#define BTN_INTERNAL_FLOOR_3_PIN GPIO_PIN_14
#define BTN_INTERNAL_FLOOR_3_PORT GPIOB

#define BTN_OPEN_DOOR_PIN GPIO_PIN_13
#define BTN_OPEN_DOOR_PORT GPIOB

// Lights
#define LIGHT_UP_PIN GPIO_PIN_12
#define LIGHT_UP_PORT GPIOB

#define LIGHT_DOWN_PIN GPIO_PIN_12
#define LIGHT_DOWN_PORT GPIOA

// Captors
#define CAPTOR_FLOOR_PIN GPIO_PIN_7
#define CAPTOR_FLOOR_PORT GPIOB

#define CAPTOR_OBSTACLE_PIN GPIO_PIN_4
#define CAPTOR_OBSTACLE_PORT GPIOC

#define CAPTOR_OPEN_DOOR_PIN GPIO_PIN_2
#define CAPTOR_OPEN_DOOR_PORT GPIOD

#define CAPTOR_CLOSED_DOOR_PIN GPIO_PIN_12
#define CAPTOR_CLOSED_DOOR_PORT GPIOC

#ifdef __cplusplus
}
#endif

typedef struct {
    int requested, distance;
} floor_command;

// States
typedef enum {
    STATE_WAITING, STATE_MOVING_UP_DOWN, STATE_OPENING_DOOR, STATE_WAITING_DOOR, STATE_CLOSING_DOOR
} states;

typedef enum {
    IDLE, UP, DOWN
} moving_states;

#endif /* __MAIN_H */

